import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { STORES_ROUTES } from './stores.routes';
import { StoresComponent } from './stores.component';
import { StoreComponent } from './store/store.component';
import { FilterComponent } from './filter/filter.component';
import { StoreCardsComponent } from '../store-cards/store-cards.component';
import { StoreCardComponent } from '../store-cards/store-card/store-card.component';
import { BadgeComponent } from '../badge/badge.component';
import { ExpiredSincePipe } from '../../pipes/expired-since.pipe';
import { CapacityColorDirective } from '../../directives/capacity-color.directive';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    StoresComponent,
    StoreComponent,
    FilterComponent,
    StoreCardsComponent,
    StoreCardComponent,
    BadgeComponent,
    ExpiredSincePipe,
    CapacityColorDirective
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(STORES_ROUTES)
  ]
})
export class StoresModule { }

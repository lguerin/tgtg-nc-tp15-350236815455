import { Routes } from '@angular/router';
import { StoresComponent } from './stores.component';
import { StoreComponent } from './store/store.component';

export const STORES_ROUTES: Routes = [
  { path: '', component: StoresComponent },
  { path: ':id', component: StoreComponent }
];

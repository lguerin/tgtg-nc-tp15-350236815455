import { Injectable } from '@angular/core';
import { StoreModel } from '../models/store.model';
import { Storage } from '../constants/storage.constants';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TgtgService {

  userFavorites: Array<string>;

  constructor(private http: HttpClient) {
    this.loadFavorites();
  }

  /**
   * Ajouter un commerce à la liste des favoris
   * @param storeId   ID du commerce à sauvegarder
   */
  addStoreToFavorites(storeId: string): void {
    if (!this.userFavorites.includes(storeId)) {
      this.userFavorites.push(storeId);
      this.storeFavorites();
    }
  }

  /**
   * Ajouter un commerce à la liste des favoris
   * @param storeId   ID du commerce à sauvegarder
   */
  removeStoreFromFavorites(storeId: string): void {
    if (this.userFavorites.includes(storeId)) {
      const index = this.userFavorites.indexOf(storeId);
      this.userFavorites.splice(index, 1);
      this.storeFavorites();
    }
  }

  /**
   * Vérifier si le commerce est un favori de l'utilisateur ou non
   * @param store Commerce
   */
  isFavorite(store: StoreModel): boolean {
    return this.userFavorites.includes(store.id);
  }

  /**
   * Switch du commerce en favori / non favori
   * @param store Commerce
   */
  toggleFavorite(store: StoreModel): boolean {
    if (this.isFavorite(store)) {
      this.removeStoreFromFavorites(store.id);
      return false;
    }
    this.addStoreToFavorites(store.id);
    return true;
  }

  private storeFavorites(): void {
    sessionStorage.setItem(Storage.USER_FAVORITES, JSON.stringify(this.userFavorites));
  }

  private loadFavorites(): void {
    this.userFavorites = [];
    const value = sessionStorage.getItem(Storage.USER_FAVORITES);
    if (value) {
      this.userFavorites = JSON.parse(value);
    }
  }

  /**
   * Récupérer toutes les données sur les commerces
   */
  getAllStores(): Observable<Array<StoreModel>> {
    return this.http.get<Array<StoreModel>>(`${environment.baseURL}/api/stores`);
  }
}
